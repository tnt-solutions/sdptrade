import { createApp } from "vue";
import App from "./App.vue";
import VueGtag from "vue-gtag";
import router from "./router";
import { Quasar } from "quasar";
import quasarUserOptions from "./quasar-user-options";
createApp(App)
  .use(Quasar, quasarUserOptions)
  .use(router)
  .use(VueGtag, {
    config: {
      id: "G-3HQRL0E3YP",
      params: {
        anonymize_ip: true,
      },
    },
  })
  .mount("#app");
