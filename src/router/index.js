import { createRouter, createWebHistory } from "vue-router";

import ContactUs from "@/views/ContactUs.vue";
import AboutUs from "@/views/AboutUs.vue";

const routes = [
  {
    path: "/",
    name: "AboutUs",
    component: AboutUs,
  },
  {
    path: "/contact-us",
    name: "ContactUs",
    component: ContactUs,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior(to) {
    if (to.hash) {
      return {
        el: to.hash,
        top: 0,
        behavior: "smooth",
      };
    }
  },
});

export default router;
